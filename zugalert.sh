#!/bin/bash

OUTPUT="$(mktemp)"

 ### First step : get cookies, ID, ...
LD=36233
SEQNR=6
IDENT=5o.025487233.1579875219
 ### second step : get the right train
 
 ### third step : get the details.
#curl "https://reiseauskunft.bahn.de/bin/query.exe/dn?ld=${LD}&protocol=https:&seqnr=${SEQNR}&ident=${IDENT}&rt=1&rememberSortType=minDeparture&HWAI=CONNECTION$C0-1!id=C0-1!HwaiConId=C0-1!HwaiDetailStatus=details!&ajax=1" --compressed
#cat ./0_example_output-details 1>${OUTPUT}
cat ./0_example_output-details | tr -d '\n' | sed -e 's/<table/\n<table/' -e 's/<tr/\n<tr/g' -e 's/<\/table>/\n<\/table>\n/' | sed -e '/<table/,/<\/table/!d' >>${OUTPUT}

 
kwrite ${OUTPUT}

rm -f ${OUTPUT}
